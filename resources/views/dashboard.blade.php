@extends('Template')
@section('Content')

<div class="main-content" style="background-color:#DCDCDC ;">
  <section class="section">
    <div class="section-header">
      <h1>Dashboard</h1>
    </div>
    <div class="row">
      <div class="col-4 col-md-6 col-lg-3">
        <div class="card">
          <canvas id="doughnutChart"></canvas>
        </div>
      </div>
      <div class="col-4 col-md-6 col-lg-3">
        <div class="card">
          <canvas id="doughnutChart1"></canvas>
        </div>
      </div>
      <div class="col-4 col-md-6 col-lg-3">
        <div class="card">
          <canvas id="doughnutChart2"></canvas>
        </div>
      </div>
      <div class="col-4 col-md-6 col-lg-3">
        <div class="card">
          <canvas id="doughnutChart3"></canvas>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-6 col-lg-6">
        
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-6 col-lg-6">
        <div class="card">
          <div class="card-body">
            <canvas id="myChart"></canvas>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-6">
        <div class="card">
          <div class="card-body">
            <canvas id="lineChart"></canvas>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h4>Indonesian Map</h4>
          </div>
          <div class="card-body">
            <div id="visitorMap3"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="row" >
      
      <div class="col-lg-6">
        <div class="card">
          <div class="card-header">
            <h4>KELAS</h4>
          </div>
          <canvas id="doughnutChartZ"></canvas>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="card">
          <div class="card-header">
            <h4>LEVEL</h4>
          </div>
            <canvas id="horizontalBarZ"></canvas>
        </div>
      </div>
      
    </div>
      <div class="row">

        <div class="col-lg-6 "height="10">
          <div class="card">
              <div class="card-header">
                <h4>USAHA</h4>
              </div> 
              <div class="card-body"  >
                <div class="mb-4" >
                  <div class="text-small float-right font-weight-bold text-muted">2,100</div>
                  <div class="font-weight-bold mb-1">KERAJINAN TANGAN</div>
                  <div class="progress" data-height="3">
                    <div class="progress-bar" role="progressbar" data-width="80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>                          
                </div>

                <div class="mb-4">
                  <div class="text-small float-right font-weight-bold text-muted">1,880</div>
                  <div class="font-weight-bold mb-1">FASHION</div>
                  <div class="progress" data-height="3">
                    <div class="progress-bar" role="progressbar" data-width="67%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>

                <div class="mb-4">
                  <div class="text-small float-right font-weight-bold text-muted">1,521</div>
                  <div class="font-weight-bold mb-1">OTOMOTIF</div>
                  <div class="progress" data-height="3">
                    <div class="progress-bar" role="progressbar" data-width="58%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>

                <div class="mb-4">
                  <div class="text-small float-right font-weight-bold text-muted">884</div>
                  <div class="font-weight-bold mb-1">ELEKTRONIK</div>
                  <div class="progress" data-height="3">
                    <div class="progress-bar" role="progressbar" data-width="36%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>

                <div class="mb-4">
                  <div class="text-small float-right font-weight-bold text-muted">473</div>
                  <div class="font-weight-bold mb-1">PROPERTI</div>
                  <div class="progress" data-height="3">
                    <div class="progress-bar" role="progressbar" data-width="28%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>

                <div class="mb-4">
                  <div class="text-small float-right font-weight-bold text-muted">418</div>
                  <div class="font-weight-bold mb-1">MAKANAN</div>
                  <div class="progress" data-height="3">
                    <div class="progress-bar" role="progressbar" data-width="20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
                <div class="mb-4">
                  <div class="text-small float-right font-weight-bold text-muted">418</div>
                  <div class="font-weight-bold mb-1">MINUMAN</div>
                  <div class="progress" data-height="3">
                    <div class="progress-bar" role="progressbar" data-width="20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
          </div>
        </div>

        <div class="col-lg-6 ">
          <div class="card">
            <canvas id="myChart99"></canvas>
          </div>
        </div>
      </div>
    
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <h4>SOSIAL MEDIA</h4>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col text-center">
              <div id="subscribers-graph1" style="height: 150px"></div>
              <div class="product-name">Facebook</div>
              </div>
              <div class="col text-center">
                <div id="subscribers-graph2" style="height: 150px"></div>
                <div class="product-name">Instagram</div>
              </div>
              <div class="col text-center">
                <div id="subscribers-graph3" style="height: 150px"></div>
                <div class="product-name">Tiktok</div>
              </div>
              <div class="col text-center">
                <div id="subscribers-graph4" style="height: 150px"></div>
                <div class="product-name">Twitter</div>
              </div>
              <div class="col text-center">
                <div id="subscribers-graph5" style="height: 150px"></div>
                <div class="product-name">WhatsApp</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <h4>MARKETPLACE</h4>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col text-center">
              <div id="subscribers-graph6" style="height: 150px"></div>
              <div class="product-name">Lazada</div>
              </div>
              <div class="col text-center">
                <div id="subscribers-graph7" style="height: 150px"></div>
                <div class="product-name">Shoope</div>
              </div>
              <div class="col text-center">
                <div id="subscribers-graph8" style="height: 150px"></div>
                <div class="product-name">Tokopedia</div>
              </div>
              <div class="col text-center">
                <div id="subscribers-graph9" style="height: 150px"></div>
                <div class="product-name">Bukalapak</div>
              </div>
              <div class="col text-center">
                <div id="subscribers-graph10" style="height: 150px"></div>
                <div class="product-name">BliBli</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@endsection