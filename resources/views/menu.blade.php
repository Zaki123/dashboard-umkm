<div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar" style="background-color:#4169E1 ;" >
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
          <div class="search-element">
            <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="250">
            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
            <div class="search-backdrop"></div>
          </div>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, kominfo@gmail.com</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.html">MANAGEMENT UMKM</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">UMKM</a>
          </div>
          
          <ul class="sidebar-menu">
            <li><a class="nav-link" href="PIU.html"><i class="far fa-square"></i> <span>PIU</span></a></li>
            <li class=active><a class="nav-link" href="index-0.html"><i class="far fa-square"></i> <span>UMKM</span></a></li>
            <li><a class="nav-link" href="AGGREGATOR.html"><i class="far fa-square"></i> <span>AGGREGATOR</span></a></li>
            <li><a class="nav-link" href="EO.html"><i class="far fa-square"></i> <span>EO</span></a></li>
            <li><a class="nav-link" href="FASILITATOR.html"><i class="far fa-square"></i> <span>FASILITATOR</span></a></li>
            <li><a class="nav-link" href="VIRTUAL EXPO.html"><i class="far fa-square"></i> <span>VIRTUAL EXPO</span></a></li>
          </ul>
          </aside>
      </div>