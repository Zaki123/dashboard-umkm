<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Dashboard UMKM</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="assets/modules/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/modules/fontawesome/css/all.min.css">

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="assets/modules/jqvmap/dist/jqvmap.min.css">
  <link rel="stylesheet" href="assets/modules/weather-icon/css/weather-icons.min.css">
  <link rel="stylesheet" href="assets/modules/weather-icon/css/weather-icons-wind.min.css">
  <link rel="stylesheet" href="assets/modules/summernote/summernote-bs4.css">

  <!-- Template CSS -->
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/css/components.css">
<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<link rel="stylesheet" href="../dist/assets/modules/popper.js">
<link rel="stylesheet" href="../dist/assets/modules/tooltip.js">
<link rel="stylesheet" href="../dist/assets/modules/summernote/summernote-bs4.js">

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
<!-- /END GA --></head>

<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      {{-- Navbar --}}
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar" style="background-color:#4169E1 ;" >
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
          <div class="search-element">
            <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="250">
            <button class="btn" type="submit"><i class="fas fa-search"></i></button>
            <div class="search-backdrop"></div>
          </div>
        </form>
        <ul class="navbar-nav navbar-right">
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, kominfo@gmail.com</div></a>
            <div class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="index.html">MANAGEMENT UMKM</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">UMKM</a>
          </div>
          
          <ul class="sidebar-menu">
            <li><a class="nav-link" href="PIU.html"><i class="far fa-square"></i> <span>PIU</span></a></li>
            <li class=active><a class="nav-link" href="index-0.html"><i class="far fa-square"></i> <span>UMKM</span></a></li>
            <li><a class="nav-link" href="AGGREGATOR.html"><i class="far fa-square"></i> <span>AGGREGATOR</span></a></li>
            <li><a class="nav-link" href="EO.html"><i class="far fa-square"></i> <span>EO</span></a></li>
            <li><a class="nav-link" href="FASILITATOR.html"><i class="far fa-square"></i> <span>FASILITATOR</span></a></li>
            <li><a class="nav-link" href="VIRTUAL EXPO.html"><i class="far fa-square"></i> <span>VIRTUAL EXPO</span></a></li>
          </ul>
          </aside>
      </div>

      {{-- akhir navbar --}}
      <!--  Content -->
      @yield('Content')
      <!-- akhir Content -->
    </div>
  </div>

  <!-- General JS Scripts -->
  <script src="http://code.highcharts.com/highcharts.js"></script>
  <script src="assets/modules/jquery.min.js"></script>
  <script src="assets/modules/popper.js"></script>
  <script src="assets/modules/tooltip.js"></script>
  <script src="assets/modules/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
  <script src="assets/modules/moment.min.js"></script>
  <script src="assets/js/stisla.js"></script>
  
  <!-- JS Libraies -->
  <script src="assets/modules/simple-weather/jquery.simpleWeather.min.js"></script>
  <script src="assets/modules/chart.min.js"></script>
  <script src="assets/modules/jqvmap/dist/jquery.vmap.min.js"></script>
  <script src="assets/modules/jqvmap/dist/maps/jquery.vmap.world.js"></script>
  <script src="assets/modules/summernote/summernote-bs4.js"></script>
  <script src="assets/modules/chocolat/dist/js/jquery.chocolat.min.js"></script>

  <!-- Page Specific JS File -->
  <script src="assets/js/page/index-0.js"></script>
  
  <!-- Template JS File -->
  <script src="assets/js/scripts.js"></script>
  <script src="assets/js/custom.js"></script>
    <!-- General JS Scripts -->
  <script src="assets/modules/jquery.min.js"></script>
  <script src="assets/modules/popper.js"></script>
  <script src="assets/modules/tooltip.js"></script>
  <script src="assets/modules/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
  <script src="assets/modules/moment.min.js"></script>
  <script src="assets/js/stisla.js"></script>
  
  <!-- JS Libraies -->
  <script src="assets/modules/izitoast/js/iziToast.min.js"></script>
  <script src="assets/modules/jqvmap/dist/jquery.vmap.min.js"></script>
  <script src="assets/modules/jqvmap/dist/maps/jquery.vmap.world.js"></script>
  <script src="assets/modules/jqvmap/dist/maps/jquery.vmap.indonesia.js"></script>

  <!-- Page Specific JS File -->
  <script src="assets/js/page/modules-vector-map.js"></script>
  
  <!-- Template JS File -->
  <script src="assets/js/scripts.js"></script>
  <script src="assets/js/custom.js"></script>

  <!-- General JS Scripts -->
  <script src="assets/modules/jquery.min.js"></script>
  <script src="assets/modules/popper.js"></script>
  <script src="assets/modules/tooltip.js"></script>
  <script src="assets/modules/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
  <script src="assets/modules/moment.min.js"></script>
  <script src="assets/js/stisla.js"></script>
  
  <!-- JS Libraies -->
  <script src="assets/modules/jquery.sparkline.min.js"></script>

  <!-- Page Specific JS File -->
  <script src="assets/js/page/modules-sparkline.js"></script>
  
  <!-- Template JS File -->
  <script src="assets/js/scripts.js"></script>
  <script src="assets/js/custom.js"></script>
  
<script>
    //doughnut
  var ctxD = document.getElementById("doughnutChart").getContext('2d');
  var myLineChart = new Chart(ctxD, {
    type: 'doughnut',
    data: {
      labels: [ "Terverifikasi"],
      datasets: [{
        data: [300, 100,],
        backgroundColor: ["#6495ED",],
      }]
    },
    options: {
      responsive: true
    }
  });
</script>

<script>
    //doughnut
  var ctxD = document.getElementById("doughnutChart1").getContext('2d');
  var myLineChart = new Chart(ctxD, {
    type: 'doughnut',
    data: {
      labels: ["Tidak Terverifikasi",],
      datasets: [{
        data: [200, 150, ],
        backgroundColor: ["#6495ED",],
        hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
      }]
    },
    options: {
      responsive: true
    }
  });
</script>

<script>
    //doughnut
  var ctxD = document.getElementById("doughnutChart2").getContext('2d');
  var myLineChart = new Chart(ctxD, {
    type: 'doughnut',
    data: {
      labels: ["Laki - Laki",],
      datasets: [{
        data: [700, 100, ],
        backgroundColor: ["#6495ED",],
        hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
      }]
    },
    options: {
      responsive: true
    }
  });
</script>

<script>
    //doughnut
  var ctxD = document.getElementById("doughnutChart3").getContext('2d');
  var myLineChart = new Chart(ctxD, {
    type: 'doughnut',
    data: {
      labels: ["Perempuan",],
      datasets: [{
        data: [700, 400, ],
        backgroundColor: ["#6495ED",],
        hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
      }]
    },
    options: {
      responsive: true
    }
  });
</script>

<script>
    new Chart(document.getElementById("horizontalBar"), {
    "type": "horizontalBar",
    "data": {
      "labels": ["BEGINNER", "Orange", "Yellow", "Green"],
      "datasets": [{
        "label": "",
        "data": [22, 33, 55, 12, 86, 23, 14],
        "fill": false,
        "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
          "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
          "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
        ],
        "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
          "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
        ],
        "borderWidth": 1
      }]
    },
    "options": {
      "scales": {
        "xAxes": [{
          "ticks": {
            "beginAtZero": true
          }
        }]
      }
    }
  });
</script>

<script>
    //line
var ctxL = document.getElementById("lineChart").getContext('2d');
var myLineChart = new Chart(ctxL, {
  type: 'line',
  data: {
    labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul","Aug","Sept","Oct","Nov","Dec"],
    datasets: [{
      label: "ASSESMENT",
      data: [65, 59, 80, 81, 56, 55,40,40,40,40,40],
      backgroundColor: [
        'rgba(105, 0, 132, .2)',
      ],
      borderColor: [
        'rgba(200, 99, 132, .7)',
      ],
      borderWidth: 2
    },
    {
      label: "PRA ASSESMENT",
      data: [28, 48, 40, 19, 86, 27, 90,55,55,55,55,55,55],
      backgroundColor: [
        'rgba(0, 137, 132, .2)',
      ],
      borderColor: [
        'rgba(0, 10, 130, .7)',
      ],
      borderWidth: 2
    }
    ]
  },
  options: {
    responsive: true
  }
});
</script>

<script>
  var xValues = [50,60,70,80,90,100,110,120,130,140,150];
  var yValues = [7,8,8,9,9,9,10,11,14,14,15];
  
  new Chart("myChart", {
    type: "line",
    data: {
      labels: xValues,
      datasets: [{
        fill: false,
        lineTension: 0,
        backgroundColor: "rgba(0,0,255,1.0)",
        borderColor: "rgba(0,0,255,0.1)",
        data: yValues
      }]
    },
    options: {
      legend: {display: false},
      scales: {
        yAxes: [{ticks: {min: 6, max:16}}],
      }
    }
  });
</script>

<script>
    const labels = [
  'KOTA 1',
  'KOTA 2',
  'KOTA 3',
  'KOTA 4',
  'KOTA 5',
  'KOTA 6',
  'KOTA 7',
  'KOTA 8',
    ];
    const data = {
      labels: labels,
      datasets: [{
        label: 'UMKM PER KAB./KOTA',
        backgroundColor: '#89CFF0',
        borderColor: '#1434A4',
        data: [0, 10, 5, 2, 20, 30, 45,20],
      }]
    };
        const config = {
      type: 'line',
      data: data,
      options: {}
    };

    var myChart = new Chart(
    document.getElementById('myChart'),
    config
  );

</script>

<Script>
    //doughnut
  var ctxD = document.getElementById("doughnutChartZ").getContext('2d');
  var myLineChart = new Chart(ctxD, {
    type: 'doughnut',
    data: {
      labels: ["MIKRO", "MENENGAH", "KECIL", ],
      datasets: [{
        data: [300, 50, 100,],
        backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", ],
        hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870",]
      }]
    },
    options: {
      responsive: true
    }
  });
</Script>

<SCript>
    new Chart(document.getElementById("horizontalBarZ"), {
    "type": "horizontalBar",
    "data": {
      "labels": ["BEGINNER", "OBSERVER", "ADOPTER", "LEADER", ],
      "datasets": [{
        "label": "My First Dataset",
        "data": [22, 33, 55, 12, ],
        "fill": false,
        "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
          "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
          "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
        ],
        "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
          "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
        ],
        "borderWidth": 1
      }]
    },
    "options": {
      "scales": {
        "xAxes": [{
          "ticks": {
            "beginAtZero": true
          }
        }]
      }
    }
  });
</SCript>

<script>
    // Our labels along the x-axis
var years = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18];
// For drawing the lines
var africa = [5,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170];
var asia = [170,160,150,140,130,120,110,100,90,80,70,60,50,40,30,20,10,5];
var europe = [150,130,110,60,100,130,170,167,140,110,150,90,160,30,170,146,234,159];
var latinAmerica = [150,130,110,60,100,130,170,167,140,110,150,90,160,30,170,146,234];
var northAmerica = [50,110,190,170,120,10,60,70,80,90,100,110,120,130,80,90,120,110];

var ctx = document.getElementById("myChart99");
var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: years,
    datasets: [
      { 
        data: africa,
        label: "BEGINNER",
        borderColor: "#3e95cd",
        fill: false
      },
      { 
        data: asia,
        label: "OBSERVER",
        borderColor: "#8e5ea2",
        fill: false
      },
      { 
        data: europe,
        label: "ADAPTOR",
        borderColor: "#3cba9f",
        fill: false
      },
      { 
        data: northAmerica,
        label: "LEADER",
        borderColor: "#c45850",
        fill: false
      }
    ]
  }
});
</script>

<script>
    //doughnut
  var ctxD = document.getElementById("doughnutChart67").getContext('2d');
  var myLineChart = new Chart(ctxD, {
    type: 'doughnut',
    data: {
      datasets: [{
        data: [300, 50,],
        backgroundColor: ["#89CFF0", "#FF5733 ", ],
        hoverBackgroundColor: ["#FF5A5E", "#5AD3D1",]
      }]
    },
    options: {
      responsive: true
    }
  });
</script>

<script>
  //doughnut
var ctxD = document.getElementById("doughnutChart68").getContext('2d');
var myLineChart = new Chart(ctxD, {
  type: 'doughnut',
  data: {
    datasets: [{
      data: [300, 50,],
      backgroundColor: ["#89CFF0", "#FF5733 ", ],
      hoverBackgroundColor: ["#FF5A5E", "#5AD3D1",]
    }]
  },
  options: {
    responsive: true
  }
});
</script>

<script>
  //doughnut
var ctxD = document.getElementById("doughnutChart69").getContext('2d');
var myLineChart = new Chart(ctxD, {
  type: 'doughnut',
  data: {
    datasets: [{
      data: [300, 50,],
      backgroundColor: ["#89CFF0", "#FF5733 ", ],
      hoverBackgroundColor: ["#FF5A5E", "#5AD3D1",]
    }]
  },
  options: {
    responsive: true
  }
});
</script>

<script>
  //doughnut
var ctxD = document.getElementById("doughnutChart70").getContext('2d');
var myLineChart = new Chart(ctxD, {
  type: 'doughnut',
  data: {
    datasets: [{
      data: [300, 50,],
      backgroundColor: ["#89CFF0", "#FF5733 ", ],
      hoverBackgroundColor: ["#FF5A5E", "#5AD3D1",]
    }]
  },
  options: {
    responsive: true
  }
});
</script>

<script>
  //doughnut
var ctxD = document.getElementById("doughnutChart71").getContext('2d');
var myLineChart = new Chart(ctxD, {
  type: 'doughnut',
  data: {
    datasets: [{
      data: [300, 50,],
      backgroundColor: ["#89CFF0", "#FF5733 ", ],
      hoverBackgroundColor: ["#FF5A5E", "#5AD3D1",]
    }]
  },
  options: {
    responsive: true
  }
});
</script>

<script>
  var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'subscribers-graph1',
        type: 'pie'
    },
    title: {
        verticalAlign: 'middle',
        floating: true,
        text: '38%',
    },
    plotOptions: {
        pie: {
            innerSize: '100%'
        },
        series: {
            states: {
                hover: {
                    enabled: false
                },
                inactive: {
                    opacity: 1
                }
            }
        }
    },
    series: [{
        borderWidth: 0,
        name: 'Subscribers',
        data: [
            {
                y: 30,
                name: "Online",
                color: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                        [0, '#4679F8'],
                        [1, '#57B2A5']
                    ]
                },
            },
            {
                y: 20,
                name: "Offline",
                color: "#DDF4E4",
            }
        ],
        size: '100%',
        innerSize: '75%',
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }],
    credits: {
        enabled: false
    }
});
</script>

<script>
  var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'subscribers-graph2',
        type: 'pie'
    },
    title: {
        verticalAlign: 'middle',
        floating: true,
        text: '50%',
    },
    plotOptions: {
        pie: {
            innerSize: '100%'
        },
        series: {
            states: {
                hover: {
                    enabled: false
                },
                inactive: {
                    opacity: 1
                }
            }
        }
    },
    series: [{
        borderWidth: 0,
        name: 'Subscribers',
        data: [
            {
                y: 30,
                name: "Online",
                color: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                        [0, '#4679F8'],
                        [1, '#57B2A5']
                    ]
                },
            },
            {
                y: 20,
                name: "Offline",
                color: "#DDF4E4",
            }
        ],
        size: '100%',
        innerSize: '75%',
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }],
    credits: {
        enabled: false
    }
});
</script>

<script>
  var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'subscribers-graph3',
        type: 'pie'
    },
    title: {
        verticalAlign: 'middle',
        floating: true,
        text: '85%',
    },
    plotOptions: {
        pie: {
            innerSize: '100%'
        },
        series: {
            states: {
                hover: {
                    enabled: false
                },
                inactive: {
                    opacity: 1
                }
            }
        }
    },
    series: [{
        borderWidth: 0,
        name: 'Subscribers',
        data: [
            {
                y: 30,
                name: "Online",
                color: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                        [0, '#4679F8'],
                        [1, '#57B2A5']
                    ]
                },
            },
            {
                y: 20,
                name: "Offline",
                color: "#DDF4E4",
            }
        ],
        size: '100%',
        innerSize: '75%',
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }],
    credits: {
        enabled: false
    }
});
</script>

<script>
  var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'subscribers-graph4',
        type: 'pie'
    },
    title: {
        verticalAlign: 'middle',
        floating: true,
        text: '25%',
    },
    plotOptions: {
        pie: {
            innerSize: '100%'
        },
        series: {
            states: {
                hover: {
                    enabled: false
                },
                inactive: {
                    opacity: 1
                }
            }
        }
    },
    series: [{
        borderWidth: 0,
        name: 'Subscribers',
        data: [
            {
                y: 30,
                name: "Online",
                color: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                        [0, '#4679F8'],
                        [1, '#57B2A5']
                    ]
                },
            },
            {
                y: 20,
                name: "Offline",
                color: "#DDF4E4",
            }
        ],
        size: '100%',
        innerSize: '75%',
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }],
    credits: {
        enabled: false
    }
});
</script>

<script>
  var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'subscribers-graph5',
        type: 'pie'
    },
    title: {
        verticalAlign: 'middle',
        floating: true,
        text: '15%',
    },
    plotOptions: {
        pie: {
            innerSize: '100%'
        },
        series: {
            states: {
                hover: {
                    enabled: false
                },
                inactive: {
                    opacity: 1
                }
            }
        }
    },
    series: [{
        borderWidth: 0,
        name: 'Subscribers',
        data: [
            {
                y: 30,
                name: "Online",
                color: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                        [0, '#4679F8'],
                        [1, '#57B2A5']
                    ]
                },
            },
            {
                y: 20,
                name: "Offline",
                color: "#DDF4E4",
            }
        ],
        size: '100%',
        innerSize: '75%',
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }],
    credits: {
        enabled: false
    }
});
</script>

<script>
  var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'subscribers-graph6',
        type: 'pie'
    },
    title: {
        verticalAlign: 'middle',
        floating: true,
        text: '38%',
    },
    plotOptions: {
        pie: {
            innerSize: '100%'
        },
        series: {
            states: {
                hover: {
                    enabled: false
                },
                inactive: {
                    opacity: 1
                }
            }
        }
    },
    series: [{
        borderWidth: 0,
        name: 'Subscribers',
        data: [
            {
                y: 30,
                name: "Online",
                color: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                        [0, '#4679F8'],
                        [1, '#57B2A5']
                    ]
                },
            },
            {
                y: 20,
                name: "Offline",
                color: "#DDF4E4",
            }
        ],
        size: '100%',
        innerSize: '75%',
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }],
    credits: {
        enabled: false
    }
});
</script>

<script>
  var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'subscribers-graph7',
        type: 'pie'
    },
    title: {
        verticalAlign: 'middle',
        floating: true,
        text: '50%',
    },
    plotOptions: {
        pie: {
            innerSize: '100%'
        },
        series: {
            states: {
                hover: {
                    enabled: false
                },
                inactive: {
                    opacity: 1
                }
            }
        }
    },
    series: [{
        borderWidth: 0,
        name: 'Subscribers',
        data: [
            {
                y: 30,
                name: "Online",
                color: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                        [0, '#4679F8'],
                        [1, '#57B2A5']
                    ]
                },
            },
            {
                y: 20,
                name: "Offline",
                color: "#DDF4E4",
            }
        ],
        size: '100%',
        innerSize: '75%',
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }],
    credits: {
        enabled: false
    }
});
</script>

<script>
  var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'subscribers-graph8',
        type: 'pie'
    },
    title: {
        verticalAlign: 'middle',
        floating: true,
        text: '85%',
    },
    plotOptions: {
        pie: {
            innerSize: '100%'
        },
        series: {
            states: {
                hover: {
                    enabled: false
                },
                inactive: {
                    opacity: 1
                }
            }
        }
    },
    series: [{
        borderWidth: 0,
        name: 'Subscribers',
        data: [
            {
                y: 30,
                name: "Online",
                color: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                        [0, '#4679F8'],
                        [1, '#57B2A5']
                    ]
                },
            },
            {
                y: 20,
                name: "Offline",
                color: "#DDF4E4",
            }
        ],
        size: '100%',
        innerSize: '75%',
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }],
    credits: {
        enabled: false
    }
});
</script>

<script>
  var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'subscribers-graph9',
        type: 'pie'
    },
    title: {
        verticalAlign: 'middle',
        floating: true,
        text: '25%',
    },
    plotOptions: {
        pie: {
            innerSize: '100%'
        },
        series: {
            states: {
                hover: {
                    enabled: false
                },
                inactive: {
                    opacity: 1
                }
            }
        }
    },
    series: [{
        borderWidth: 0,
        name: 'Subscribers',
        data: [
            {
                y: 30,
                name: "Online",
                color: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                        [0, '#4679F8'],
                        [1, '#57B2A5']
                    ]
                },
            },
            {
                y: 20,
                name: "Offline",
                color: "#DDF4E4",
            }
        ],
        size: '100%',
        innerSize: '75%',
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }],
    credits: {
        enabled: false
    }
});
</script>

<script>
  var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'subscribers-graph10',
        type: 'pie'
    },
    title: {
        verticalAlign: 'middle',
        floating: true,
        text: '12%',
    },
    plotOptions: {
        pie: {
            innerSize: '100%'
        },
        series: {
            states: {
                hover: {
                    enabled: false
                },
                inactive: {
                    opacity: 1
                }
            }
        }
    },
    series: [{
        borderWidth: 0,
        name: 'Subscribers',
        data: [
            {
                y: 30,
                name: "Online",
                color: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                        [0, '#4679F8'],
                        [1, '#57B2A5']
                    ]
                },
            },
            {
                y: 20,
                name: "Offline",
                color: "#DDF4E4",
            }
        ],
        size: '100%',
        innerSize: '75%',
        showInLegend: false,
        dataLabels: {
            enabled: false
        }
    }],
    credits: {
        enabled: false
    }
});
</script>
</body>
</html>